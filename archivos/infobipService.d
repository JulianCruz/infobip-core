# SHELL INFOBIP SERVICE
echo "INFOBIP SERVICES - Brainwinner CORE 1.0 Based..."
#export JAVA_HOME=/usr/lib/jvm/java-1.6.0-openjdk
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-i386
export PATH=$PATH:$JAVA_HOME/bin
fecha=`date '+%Y%m%d%H%M%S'`
DAEMON_HOME=/usr/bin
COMPANY=/infobip-core
PID_FILE=/opt$COMPANY/infobipservice.pid
HOME=/opt$COMPANY
HOMELIB=$HOME/lib
HOMECONFIG=$HOME/
CONFIGFILE=$HOME/transactional.properties
	SERVICE=INFOBIP
USER=root
RETVAL=0
PORT=28601
TMP_DIR=/var/tmp
CLASSPATH=.:$HOME/infobip-core.jar:$HOME/infobip.properties:$HOMECONFIG
for lib in $HOMELIB/*; do
        CLASSPATH=$CLASSPATH:$lib
done
CLASSPATH=$CLASSPATH:$HOMELIB/
case "$1" in
        start)
        echo "Iniciar Servicio  - Infobip"
        $DAEMON_HOME/jsvc \
        -home $JAVA_HOME \
        -Djava.io.tmpdir=$TMP_DIR \
        -Dconfig.port=$PORT \
	-Dconfig.service=$SERVICE \
	-Dconfig.file=$CONFIGFILE \
        -pidfile $PID_FILE \
        -outfile $HOME/log.out \
        -errfile $HOME/log.err \
        -cp $CLASSPATH \
        co.puntored.infobip.core.init.InfobipService
        chmod 777 $HOME/log.out
        chmod 777 $HOME/log.err
        echo "done"
        exit $?;;

        stop)
        echo "Deteniendo Servicio - Infobip"
        $DAEMON_HOME/jsvc \
        -stop \
        -pidfile $PID_FILE \
        co.puntored.infobip.core.init.InfobipService
        mv $HOME/log.out $HOME/log.$fecha.out
        mv $HOME/log.err $HOME/log.$fecha.err
        echo "done"
        exit $?
        ;;
*)    
echo "Use start/stop"
    exit 1;;
esac
