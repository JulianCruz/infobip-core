package co.puntored.infobip.core.properties;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadPropertiesFile {
	static File archivo = null;
    static Properties prop = null;
    static FileInputStream fis = null;
    static ReadPropertiesFile instance = null;


    private ReadPropertiesFile() {
        openPropertiesFile();
    }

    private ReadPropertiesFile(String file) {
        openPropertiesFile(file);
    }

    public static ReadPropertiesFile getInstance(){
        if(instance == null)
            instance = new ReadPropertiesFile();
        return instance;
    }

    public static ReadPropertiesFile getInstance(String file){
        if(instance == null)
            instance = new ReadPropertiesFile(file);
        return instance;
    }

    public static String getProperty(String key) {
        String value = null;
        value = prop.getProperty(key);
//        System.out.println(key+"="+value);
        return value;
    }

    private static void openPropertiesFile() {
        try {
            archivo = new File("/opt/infobip-core/infobip.properties");
//            archivo = new File("A:/routes.pro");
            prop = new Properties();
            fis = new FileInputStream(archivo);
            prop.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void openPropertiesFile(String file) {
        try {
            prop = new Properties();
            fis = new FileInputStream(file);
            prop.load(fis);
//            System.out.println(prop);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}