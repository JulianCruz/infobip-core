/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.properties;

import java.util.ResourceBundle;

/**
 *
 * @author JCruz
 */
public class ReadProperties{
    
  
  public static String getProperty(String name)
  {
    String property = null;
    try
    {
//        ResourceBundle rb = ResourceBundle.getBundle("co.puntored.infobip.core.properties.infobip");
        ResourceBundle rb = ResourceBundle.getBundle("infobip");
        property = rb.getString(name);      
    }catch (Exception e)
    {
      e.printStackTrace();
    }
    return property;
  }
    
}
