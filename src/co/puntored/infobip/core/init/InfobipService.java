/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.init;

/**
 *
 * @author JCruz
 */
import co.puntored.infobip.core.listener.InfobipListener;
import co.puntored.infobip.core.properties.ReadProperties;
import co.puntored.infobip.core.server.BWGServer_1;
import java.io.PrintStream;
import java.util.Timer;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.jpos.iso.ISOMsg;

public class InfobipService implements Daemon
{
  private BWGServer_1 server = null;
  ReadProperties readProperties;
  Timer timer = new Timer();
  ISOMsg responseFromSingOn = null;
  ISOMsg responseFromDke = null;
  
  public void init(DaemonContext dc)
    throws DaemonInitException, Exception
  {
    System.out.println("Init Infobip Service");
    this.readProperties = new ReadProperties();
  }
  
  public void start()
    throws Exception
  {
    System.out.println("Starting Infobip Service");
    this.server = new BWGServer_1(Integer.parseInt(System.getProperty("config.port")), new InfobipListener());
    this.server.init();
  }
  
  public void stop()
    throws Exception
  {
    System.out.println("Stoping Infobip Service");
    if (this.server != null) {
      this.server.stop();
    }
    if (this.timer != null) {
      this.timer.cancel();
    }
  }
  
  public void destroy()
  {
    System.out.println("Destroying Infobip Service");
  }
    
}
