/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.listener;

import co.puntored.infobip.api.response.ResponseInfobip;
import co.puntored.infobip.core.properties.ReadProperties;
import co.puntored.infobip.service.InfobipServices;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOFilter;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.ThreadPool;
/**
 *
 * @author JCruz
 */
public class InfobipListener implements ISORequestListener
{
  private static final int MIN_THREAD = 10;
  private static final int MAX_THREAD = 20;
  private final ThreadPool poolMessage = new ThreadPool(MIN_THREAD, MAX_THREAD);
  InfobipServices infobipService;
  private ReadProperties prop;
  Date date = null;
  SimpleDateFormat sdf = null;
  String host = null;
  String user = null;
  String password = null;
  ResponseInfobip responseInfobip;
  
  public InfobipListener()
  {
    this.prop = new ReadProperties();
  }
  
  public boolean process(ISOSource source, ISOMsg msg)
  {
    boolean estado = false;
    try
    {
      this.poolMessage.execute(new Process(source, msg));
    }
    catch (Exception e)
    {
        System.out.println("Infobip Listener catch 1");
      e.printStackTrace();
    }
    return estado;
  }
  
  class Process
    implements Runnable
  {
    private ISOSource source = null;
    private ISOMsg msg = null;
    
    public Process(ISOSource source, ISOMsg msg)
    {
      this.source = source;
      this.msg = msg;
      InfobipListener.this.infobipService = new InfobipServices();
    }
    
    public void run()
    {
      ISOMsg response = null;
      int MTI = 0;
      int pcode = 0;
      try
      {
        response = (ISOMsg)this.msg.clone();
        response.setResponseMTI();
        MTI = Integer.parseInt(this.msg.getMTI());
        System.out.println("MTI " + MTI);
        switch (MTI)
        {
        case 220: 
          pcode = Integer.parseInt(this.msg.getString(3));
          System.out.println("PCODE " + pcode);
          switch (pcode)
          {
          case 1: 
            try
            {
              InfobipListener.this.responseInfobip = InfobipListener.this.infobipService.sendPost(this.msg.getString(63).trim(), this.msg.getString(123).trim());
              System.out.println("Respuesta completa: "+InfobipListener.this.responseInfobip);
              if (InfobipListener.this.responseInfobip.isStatus())
              {
                response.set(new ISOField(39, "00"));
//                response.set(new ISOField(37, InfobipListener.this.responseInfobip.getId()));
                response.set(new ISOField(37, "4"));
                response.set(new ISOField(63, InfobipListener.this.responseInfobip.getMessage()));
                response.set(new ISOField(123, InfobipListener.this.responseInfobip.getCode()));
              }
              else
              {
                response.set(new ISOField(39, "99"));
                response.set(new ISOField(37, "4"));
                response.set(new ISOField(63, InfobipListener.this.responseInfobip.getMessage()));
                response.set(new ISOField(123, InfobipListener.this.responseInfobip.getCode()));
                System.out.println("GetMessage"+InfobipListener.this.responseInfobip.getMessage());
                System.out.println("GetCode"+InfobipListener.this.responseInfobip.getCode());
              }
            }
            catch (Exception ex)
            {
              response.set(new ISOField(39, "99"));
              response.set(new ISOField(37, "4"));
              response.set(new ISOField(63, "NO HUBO RESPUESTA DEL AUTORIZADOR"));
              System.out.println("Infobip Listener catch 2");
            }
          }
          break;
        }
        this.source.send(response);
      }
      catch (ISOException|NumberFormatException e)
      {
          e.printStackTrace();
          System.out.println("Infobip Listener catch 3");
        try
        {
          response = (ISOMsg)this.msg.clone();
          response.setResponseMTI();
          response.set(39, "99");
          response.set(new ISOField(37, "4"));
          response.set(63, e.getMessage().substring(0, 100));
          this.source.send(response);
        }
        catch (IOException ex)
        {
          ex.printStackTrace();
          System.out.println("Infobip Listener catch 3");
        }
        catch (ISOFilter.VetoException ex)
        {
          ex.printStackTrace();
          System.out.println("Infobip Listener catch 4");
        }
        catch (ISOException ex)
        {
          ex.printStackTrace();
          System.out.println("Infobip Listener catch 5");
        }
        e.printStackTrace();
      }
      catch (IOException ex)
      {
        ex = 
        
          ex;Logger.getLogger(InfobipListener.class.getName()).log(Level.SEVERE, null, ex);
          System.out.println("Infobip Listener catch 6");
      }
      finally {}
    }
  }
    
}
