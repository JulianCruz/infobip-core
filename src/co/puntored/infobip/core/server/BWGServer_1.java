/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.server;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import org.jpos.util.ThreadPool;
/**
 *
 * @author JCruz
 */
public class BWGServer_1 {    
    
  private Thread proceso = null;
  private ISOServer server = null;
  private int port = 0;
  ISORequestListener listener = null;
  private final int MIN_THREAD = 10;
  private final int MAX_THREAD = 20;
  
  public BWGServer_1() {}
  
  public BWGServer_1(int port, ISORequestListener listener)
  {
    this.port = port;
    this.listener = listener;
  }
  
  public void init()
  {
    PrintStream out = null;
    SimpleDateFormat sdf = null;
    try
    {
      Logger logger = new Logger();
      ISOChannel clientSideChannel = new ASCIIChannel(new ISO87APackager());
      ThreadPool pool = null;
      sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
      out = new PrintStream(new FileOutputStream("/var/log/" + System.getProperty("config.service") + "_" + sdf.format(new Date()) + ".log"));
      logger.addListener(new SimpleLogListener(out));
      
      pool = new ThreadPool(MIN_THREAD, MAX_THREAD);
      pool.setLogger(logger, "iso-pool-efecty");
      this.server = new ISOServer(this.port, (ServerChannel)clientSideChannel, pool);
      this.server.setLogger(logger, "iso-server-efecty");
      this.server.addISORequestListener(this.listener);
      this.proceso = new Thread(this.server);
      this.proceso.start();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void stop()
  {
    if (this.server != null) {
      this.server.shutdown();
    }
  }
}
