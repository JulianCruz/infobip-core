/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOBasePackager;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class AbstractServices <T extends BaseChannel, P extends ISOBasePackager>
{
  private Class<? extends BaseChannel> channelClass = null;
  private Class<? extends ISOBasePackager> packagerClass = null;
  private T channel = null;
  protected Properties props = null;
  protected String host = null;
  protected int port = 0;
  
  public T getInstanceOfChannel()
    throws RuntimeException
  {
    try
    {
      T instance = (T)(BaseChannel)this.channelClass.newInstance();
      instance.setHost(this.host);
      instance.setPort(this.port);
      System.out.println("INFOBIP ISO :" + this.host + ":" + this.port);
      instance.setPackager(getInstanceOfPackager());
      return instance;
    }
    catch (InstantiationException ex)
    {
      Logger.getLogger(AbstractServices.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      Logger.getLogger(AbstractServices.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }
  
  private P getInstanceOfPackager()
    throws RuntimeException
  {
    try
    {
      return (P)(ISOBasePackager)this.packagerClass.newInstance();
    }
    catch (InstantiationException ex)
    {
      Logger.getLogger(AbstractServices.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      Logger.getLogger(AbstractServices.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }
  
  public AbstractServices(Class<? extends BaseChannel> channelClass, Class<? extends ISOBasePackager> packagerClass, String propertiesFile)
  {
    this.channelClass = channelClass;
    this.packagerClass = packagerClass;
    FileInputStream in = null;
    try
    {
      this.props = new Properties();
      in = new FileInputStream(propertiesFile);
      this.props.load(in);
      in.close();
    }
    catch (IOException e)
    {
      try
      {
        InputStream inn = null;
        this.props = new Properties();
        File file = new File(propertiesFile);
        System.out.println("Loading:/co/puntored/infobip/core/properties/" + file.getName());
        inn = getClass().getResourceAsStream("/co/puntored/infobip/core/properties/" + file.getName());
        this.props.load(inn);
        inn.close();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
    finally
    {
      setProperties(this.props);
    }
  }
  
  public final void setProperties(Properties props)
  {
    this.props = props;
  }
  
  public ISOMsg send(ISOMsg msg)
    throws IOException
  {
    ISOMsg response = null;
    try
    {
      if ((this.channel != null) && (this.channel.isConnected()))
      {
        this.channel.send(msg);
        response = this.channel.receive();
        disconnect();
        this.channel = null;
      }
      else
      {
        connect();
        this.channel.send(msg);
        response = this.channel.receive();
        disconnect();
        this.channel = null;
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      System.out.println("Disconnect");
      disconnect();
      if (this.channel != null) {
        this.channel.disconnect();
      }
    }
    return response;
  }
  
  public boolean connect()
  {
    boolean status = false;
    int timeout = 0;
    try
    {
      this.host = this.props.getProperty("infobip.host");
      this.port = Integer.parseInt(this.props.getProperty("infobip.port"));
      timeout = Integer.parseInt(this.props.getProperty("infobip.timeout"));
      this.channel = getInstanceOfChannel();
      this.channel.setTimeout(timeout);
      this.channel.connect();
      status = this.channel.isConnected();
    }
    catch (Exception e)
    {
      Logger.getLogger(AbstractServices.class.toString()).severe(e.getMessage());
    }
    return status;
  }
  
  public void disconnect()
  {
    try
    {
      if (this.channel != null)
      {
        System.out.println("Disconnecting.....");
        this.channel.disconnect();
        this.channel = null;
      }
    }
    catch (IOException e)
    {
      Logger.getLogger(AbstractServices.class.toString()).severe(e.getMessage());
    }
  }
}

    

