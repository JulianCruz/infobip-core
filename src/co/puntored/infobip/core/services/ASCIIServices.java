/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.core.services;

import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.ISO87APackager;
/**
 *
 * @author JCruz
 */
public class ASCIIServices extends AbstractServices<ASCIIChannel, ISO87APackager>
{
  public ASCIIServices()
  {
    super(ASCIIChannel.class, ISO87APackager.class, "/opt/infobip-core/infobip.properties");
  }
    
}
