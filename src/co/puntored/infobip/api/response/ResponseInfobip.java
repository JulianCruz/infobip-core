/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.api.response;


import java.io.Serializable;
/**
 *
 * @author JCruz
 */
public class ResponseInfobip implements Serializable{    
  private boolean status = false;
  private String message = "NO SE RECIBIO RESPUESTA O INFOBIP ENVIO UNA RESPUESTA INVALIDA";
  private String code = "-1";
  private String id = null;
  private String groupName = null;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
  
  public boolean isStatus()
  {
    return this.status;
  }
  
  public void setStatus(boolean status)
  {
    this.status = status;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
  
  public String getCode()
  {
    return this.code;
  }
  
  public void setCode(String code)
  {
    this.code = code;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public void setId(String id)
  {
    this.id = id;
  }

    @Override
    public String toString() {
        return "ResponseInfobip{" + "status=" + status + ", message=" + message + ", code=" + code + ", id=" + id + ", groupName=" + groupName + '}';
    }

   
  
  
}
