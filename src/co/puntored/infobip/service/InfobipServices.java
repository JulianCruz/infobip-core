/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.puntored.infobip.service;

import co.puntored.infobip.api.response.ResponseInfobip;
import co.puntored.infobip.core.properties.ReadPropertiesFile;
import com.brainwinner.util.LogSocketListener;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.HttpsURLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author JCruz
 */
public class InfobipServices {

    private String url = null;
    private final String USER_AGENT = "Mozilla/5.0";
    boolean debug = true;
    private static LogSocketListener log = null;
    private static final String LOG_NAME = "INFOBIP_CORE_API";

    public InfobipServices() {
        getLog();
    }

    private static LogSocketListener getLog() {
        if (log == null) {
            PrintStream out = null;
            SimpleDateFormat sdf = null;
            try {
                sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
                out = new PrintStream(new FileOutputStream("/var/local/core/logs/INFOBIP_CORE_APnI_" + sdf.format(new Date()) + ".log"));
                log = new LogSocketListener(out);
            } catch (Exception e) {
                log = new LogSocketListener();
                e.printStackTrace();
            }
        }
        return log;
    }

    public ResponseInfobip sendPost(String mobile, String message)
            throws Exception {
        ResponseInfobip responseInfobip = new ResponseInfobip();
        if (this.debug) {
            log.log("\n\n");
            java.util.Date fecha = new Date();
            log.log("Fecha: "+fecha);
            log.log("Sending message to " + mobile);
        }
        try {
            ReadPropertiesFile lector = ReadPropertiesFile.getInstance();
            String url = lector.getProperty("infobip.url");
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            String usuarioPuntored = lector.getProperty("infobip.user");
            
            
            //Configuración del body
            String urlParameters = "{" +
            "\"from\":\""+usuarioPuntored+"\"," +
            "\"to\":\""+"57"+mobile+"\","+
            "\"text\":\""+message+"\"" 
             +"}";

            //se da formato al body
            byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8 );
            int postDataLength = postData.length;
            
            //Configuración del header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            con.setRequestProperty("Authorization", lector.getProperty("infobip.auth"));
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        
            //Envío del mensaje 
            log.log("\nSending 'POST' request to URL : " + this.url);
            log.log("\nSending Json : " + urlParameters);
            
            //Recupero la respuesta del servidor Infobip
            con.getOutputStream().write(postData);
            Reader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0;){
                sb.append((char)c);
            }
            int responseCode = con.getResponseCode();
            String response = con.getResponseMessage();
            String responseJSON = sb.toString();
            
            log.log("Response Code : " + responseCode);
            log.log("Response Message : " + response);
            log.log("Respuesta Infobip Recarga: "+responseJSON.toString());
            responseInfobip = parser(responseJSON);
            if(responseCode == 200 && responseInfobip.getGroupName().equals("PENDING")){
                responseInfobip.setStatus(true);
                responseInfobip.setCode("00");
                log.log("Response: "+responseInfobip);
                log.log("++++++++++Mensaje Exitoso !!!++++++++++");
                log.log("\n\n");
            }else {
                responseInfobip.setStatus(false);
                responseInfobip.setCode("99");
                log.log("Response: "+responseInfobip);
                log.log("----------Mensaje Fallido !!!----------");
                log.log("\n\n");
            }    
            } catch (Exception e) {
                responseInfobip.setStatus(false);
                responseInfobip.setCode("98");
                responseInfobip.setMessage(e.getMessage());
                log.log("Response: "+responseInfobip);
                log.log("Error en infobip api :" + e.getMessage());
                log.log("\n\n");
        }         
        return responseInfobip;
    }
    
    private ResponseInfobip parser(String json) throws ParseException{
        
        ResponseInfobip response = new ResponseInfobip();
        JSONParser parserJSON = new JSONParser();
        
        //Convertir el String a objeto JSON
        JSONObject allMessage = (JSONObject) parserJSON.parse(json);
        
        //Recupero la respuesta del unico mensaje enviado
        JSONArray message = (JSONArray) allMessage.get("messages");
        JSONObject message1JSON = (JSONObject) message.get(0);
        
        //Recupero el subJSON de status, para tomar la descripción
        JSONObject statusJSON = (JSONObject) message1JSON.get("status");
        
        //Asigno los valores para el response de respuesta
        response.setId((String) message1JSON.get("messageId"));
        response.setMessage((String) statusJSON.get("description"));
        response.setGroupName((String) statusJSON.get("groupName"));
        return response;
    }

    public static void main(String[] args) {
//        InfobipServices service = new InfobipServices("https://apismsi.aldeamo.com/smsr/r/hcws/smsSendPost", "Puntored", "Punt0SMS**");
        InfobipServices service = new InfobipServices();
        ResponseInfobip response = null;
        getLog();
        try {
            response = service.sendPost("3124528533", "Test desde core intermedio");
            if (response.isStatus()) {
                System.out.println(response.getId() + "-" + response.getMessage());
            } else {
                System.out.println(response.getCode() + "-" + response.getMessage());
            }
            log.log(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
